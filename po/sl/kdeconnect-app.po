# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeconnect-kde package.
#
# Matjaž Jeran <matjaz.jeran@amis.net>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-10 01:34+0000\n"
"PO-Revision-Date: 2023-09-16 09:39+0200\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"X-Generator: Poedit 3.3.2\n"

#: main.cpp:32 main.cpp:34
#, kde-format
msgid "KDE Connect"
msgstr "KDE Connect"

#: main.cpp:36
#, kde-format
msgid "(c) 2015, Aleix Pol Gonzalez"
msgstr "(c) 2015, Aleix Pol Gonzalez"

#: main.cpp:37
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr "Aleix Pol Gonzalez"

#: main.cpp:37
#, kde-format
msgid "Maintainer"
msgstr "Vzdrževalec"

#: main.cpp:38
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Matjaž Jeran"

#: main.cpp:38
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "matjaz.jeran@amis.net"

#: main.cpp:55
#, kde-format
msgid "URL to share"
msgstr "URL za deljenje"

#: qml/DevicePage.qml:24
#, kde-format
msgid "Unpair"
msgstr "Loči par"

#: qml/DevicePage.qml:29
#, kde-format
msgid "Send Ping"
msgstr "Pošlji ping"

#: qml/DevicePage.qml:37 qml/PluginSettings.qml:15
#, kde-format
msgid "Plugin Settings"
msgstr "Nastavitve vtičnika"

#: qml/DevicePage.qml:61
#, kde-format
msgid "Multimedia control"
msgstr "Nadzor večpredstavnosti"

#: qml/DevicePage.qml:68
#, kde-format
msgid "Remote input"
msgstr "Oddaljeni input"

#: qml/DevicePage.qml:75 qml/presentationRemote.qml:15
#, kde-format
msgid "Presentation Remote"
msgstr "Oddaljena predstavitev"

#: qml/DevicePage.qml:84 qml/mousepad.qml:44
#, kde-format
msgid "Lock"
msgstr "Zakleni"

#: qml/DevicePage.qml:84
#, kde-format
msgid "Unlock"
msgstr "Odkleni"

#: qml/DevicePage.qml:91
#, kde-format
msgid "Find Device"
msgstr "Poišči napravo"

#: qml/DevicePage.qml:96 qml/runcommand.qml:16
#, kde-format
msgid "Run command"
msgstr "Zaženi ukaz"

#: qml/DevicePage.qml:105
#, kde-format
msgid "Send Clipboard"
msgstr "Pošlji odložišče"

#: qml/DevicePage.qml:111
#, kde-format
msgid "Share File"
msgstr "Deli datoteko"

#: qml/DevicePage.qml:116 qml/volume.qml:16
#, kde-format
msgid "Volume control"
msgstr "Nadzor glasnosti"

#: qml/DevicePage.qml:125
#, kde-format
msgid "This device is not paired"
msgstr "Ta naprava ni uparjena"

#: qml/DevicePage.qml:129
#, kde-format
msgctxt "Request pairing with a given device"
msgid "Pair"
msgstr "Upari"

#: qml/DevicePage.qml:136 qml/DevicePage.qml:145
#, kde-format
msgid "Pair requested"
msgstr "Zahtevan par"

#: qml/DevicePage.qml:151
#, kde-format
msgid "Accept"
msgstr "Sprejmi"

#: qml/DevicePage.qml:157
#, kde-format
msgid "Reject"
msgstr "Zavrni"

#: qml/DevicePage.qml:166
#, kde-format
msgid "This device is not reachable"
msgstr "Ta naprava ni dosegljiva"

#: qml/DevicePage.qml:174
#, kde-format
msgid "Please choose a file"
msgstr "Izberite datoteko"

#: qml/FindDevicesPage.qml:23
#, kde-format
msgctxt "Title of the page listing the devices"
msgid "Devices"
msgstr "Naprave"

#: qml/FindDevicesPage.qml:38
#, kde-format
msgid "No devices found"
msgstr "Ni najdenih naprav"

#: qml/FindDevicesPage.qml:54
#, kde-format
msgid "Remembered"
msgstr "Zapomnjena"

#: qml/FindDevicesPage.qml:56
#, kde-format
msgid "Available"
msgstr "Na voljo"

#: qml/FindDevicesPage.qml:58
#, kde-format
msgid "Connected"
msgstr "Povezana"

#: qml/Main.qml:73
#, kde-format
msgid "Find devices..."
msgstr "Poišči naprave..."

#: qml/Main.qml:122 qml/Main.qml:126
#, kde-format
msgid "Settings"
msgstr "Nastavitve"

#: qml/mousepad.qml:16
#, kde-format
msgid "Remote Control"
msgstr "Oddaljen nadzor"

#: qml/mousepad.qml:59
#, kde-format
msgid "Press %1 or the left and right mouse buttons at the same time to unlock"
msgstr "Pritisnite %1 ali sočasno levi in desni gumb miške, da odklenete"

#: qml/mpris.qml:19
#, kde-format
msgid "Multimedia Controls"
msgstr "Nadzor večpredstavnosti"

#: qml/mpris.qml:65
#, kde-format
msgid "No players available"
msgstr "Ni na voljo predvajalnikov"

#: qml/mpris.qml:105
#, kde-format
msgid "%1 - %2"
msgstr "%1 - %2"

#: qml/presentationRemote.qml:21
#, kde-format
msgid "Enable Full-Screen"
msgstr "Omogoči celoten zaslon"

#: qml/runcommand.qml:22
#, kde-format
msgid "Edit commands"
msgstr "Ukazi urejanja"

#: qml/runcommand.qml:25
#, kde-format
msgid "You can edit commands on the connected device"
msgstr "Lahko urejate ukaze na povezani napravi"

#: qml/runcommand.qml:50
#, kde-format
msgid "No commands defined"
msgstr "Ni določenega ukaza"

#: qml/Settings.qml:13
#, kde-format
msgctxt "@title:window"
msgid "Settings"
msgstr "Nastavitve"

#: qml/Settings.qml:21
#, kde-format
msgid "Device name"
msgstr "Ime naprave"

#: qml/Settings.qml:36
#, kde-format
msgid "About KDE Connect"
msgstr "O KDE Connectu"

#: qml/Settings.qml:47
#, kde-format
msgid "About KDE"
msgstr "O KDE"
