# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the kdeconnect-kde package.
#
# SPDX-FileCopyrightText: 2022, 2023, 2024 Zayed Al-Saidi <zayed.alsaidi@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-10 01:34+0000\n"
"PO-Revision-Date: 2024-01-17 17:21+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: ar\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 23.08.1\n"

#: main.cpp:32 main.cpp:34
#, kde-format
msgid "KDE Connect"
msgstr "كِيدِي المتّصل"

#: main.cpp:36
#, kde-format
msgid "(c) 2015, Aleix Pol Gonzalez"
msgstr "© 2015 أليكس بول غونزاليس"

#: main.cpp:37
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr "أليكس بول غونزاليس"

#: main.cpp:37
#, kde-format
msgid "Maintainer"
msgstr "المصين"

#: main.cpp:38
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "زايد السعيدي"

#: main.cpp:38
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "zayed.alsaidi@gmail.com"

#: main.cpp:55
#, kde-format
msgid "URL to share"
msgstr "الربط للمشاركة"

#: qml/DevicePage.qml:24
#, kde-format
msgid "Unpair"
msgstr "أزل الاقتران"

#: qml/DevicePage.qml:29
#, kde-format
msgid "Send Ping"
msgstr "أرسل وخزة"

#: qml/DevicePage.qml:37 qml/PluginSettings.qml:15
#, kde-format
msgid "Plugin Settings"
msgstr "أوضاع البرنامج المساعد"

#: qml/DevicePage.qml:61
#, kde-format
msgid "Multimedia control"
msgstr "تحكّمات الوسائط المتعدّدة"

#: qml/DevicePage.qml:68
#, kde-format
msgid "Remote input"
msgstr "الدّخل البعيد"

#: qml/DevicePage.qml:75 qml/presentationRemote.qml:15
#, kde-format
msgid "Presentation Remote"
msgstr "متحكم العرض التقديمي"

#: qml/DevicePage.qml:84 qml/mousepad.qml:44
#, kde-format
msgid "Lock"
msgstr "اقفل"

#: qml/DevicePage.qml:84
#, kde-format
msgid "Unlock"
msgstr "فكّ القفل"

#: qml/DevicePage.qml:91
#, kde-format
msgid "Find Device"
msgstr "اعثر على جهاز"

#: qml/DevicePage.qml:96 qml/runcommand.qml:16
#, kde-format
msgid "Run command"
msgstr "نفّذ أمرًا"

#: qml/DevicePage.qml:105
#, kde-format
msgid "Send Clipboard"
msgstr "أرسل الحافظة"

#: qml/DevicePage.qml:111
#, kde-format
msgid "Share File"
msgstr "شارك ملف"

#: qml/DevicePage.qml:116 qml/volume.qml:16
#, kde-format
msgid "Volume control"
msgstr "تحكم بالصوت"

#: qml/DevicePage.qml:125
#, kde-format
msgid "This device is not paired"
msgstr "الجهاز غير مقترن"

#: qml/DevicePage.qml:129
#, kde-format
msgctxt "Request pairing with a given device"
msgid "Pair"
msgstr "اقرن"

#: qml/DevicePage.qml:136 qml/DevicePage.qml:145
#, kde-format
msgid "Pair requested"
msgstr "طُلب الاقتران"

#: qml/DevicePage.qml:151
#, kde-format
msgid "Accept"
msgstr "اقبل"

#: qml/DevicePage.qml:157
#, kde-format
msgid "Reject"
msgstr "ارفض"

#: qml/DevicePage.qml:166
#, kde-format
msgid "This device is not reachable"
msgstr "الجهاز غير قابل الوصول"

#: qml/DevicePage.qml:174
#, kde-format
msgid "Please choose a file"
msgstr "رجاءً اختر ملفا"

#: qml/FindDevicesPage.qml:23
#, kde-format
msgctxt "Title of the page listing the devices"
msgid "Devices"
msgstr "الأجهزة"

#: qml/FindDevicesPage.qml:38
#, kde-format
msgid "No devices found"
msgstr "لم يُعثر على أجهزة"

#: qml/FindDevicesPage.qml:54
#, kde-format
msgid "Remembered"
msgstr "متذكَّرة"

#: qml/FindDevicesPage.qml:56
#, kde-format
msgid "Available"
msgstr "متوفّرة"

#: qml/FindDevicesPage.qml:58
#, kde-format
msgid "Connected"
msgstr "متّصل"

#: qml/Main.qml:73
#, kde-format
msgid "Find devices..."
msgstr "اعثر على أجهزة…"

#: qml/Main.qml:122 qml/Main.qml:126
#, kde-format
msgid "Settings"
msgstr "إعدادات"

#: qml/mousepad.qml:16
#, kde-format
msgid "Remote Control"
msgstr "جهاز التحكم عن بعد"

#: qml/mousepad.qml:59
#, kde-format
msgid "Press %1 or the left and right mouse buttons at the same time to unlock"
msgstr "اضغط %1 أو زري الأيمن والأيسر للفأرة في نفس الوقت لفك القفل"

#: qml/mpris.qml:19
#, kde-format
msgid "Multimedia Controls"
msgstr "تحكّمات الوسائط المتعدّدة"

#: qml/mpris.qml:65
#, kde-format
msgid "No players available"
msgstr "لا تتوفر أي مشغلات"

#: qml/mpris.qml:105
#, kde-format
msgid "%1 - %2"
msgstr "%1 - %2"

#: qml/presentationRemote.qml:21
#, kde-format
msgid "Enable Full-Screen"
msgstr "مكن ملء الشاشة"

#: qml/runcommand.qml:22
#, kde-format
msgid "Edit commands"
msgstr "تحرير الأوامر"

#: qml/runcommand.qml:25
#, kde-format
msgid "You can edit commands on the connected device"
msgstr "يمكنك تحرير الأوامر على الجهاز المتصل"

#: qml/runcommand.qml:50
#, kde-format
msgid "No commands defined"
msgstr "لم يُحدّد إجراء"

#: qml/Settings.qml:13
#, kde-format
msgctxt "@title:window"
msgid "Settings"
msgstr "إعدادات"

#: qml/Settings.qml:21
#, kde-format
msgid "Device name"
msgstr "اسم الجهاز"

#: qml/Settings.qml:36
#, kde-format
msgid "About KDE Connect"
msgstr "عن كِيدِي المتّصل"

#: qml/Settings.qml:47
#, kde-format
msgid "About KDE"
msgstr "عن كِيدِي"
